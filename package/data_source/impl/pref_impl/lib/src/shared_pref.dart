import 'dart:async';

import 'package:core/export.dart';
import 'package:data_api/export.dart';
import 'package:shared_preferences/shared_preferences.dart';

abstract class SharedPref<T> extends DataController<T?> implements Disposable {
  late final _streamCtrl = StreamController<T?>.broadcast();
  final Future<bool> Function(String, T) valueSetter;
  final T? Function(String) valueGetter;
  final SharedPreferences sharedPreferences;
  final String key;

  SharedPref(
    this.sharedPreferences,
    this.key,
    this.valueSetter,
    this.valueGetter,
  );

  @override
  void setValue(T? value) {
    if (value == null) {
      sharedPreferences.remove(key);
    } else {
      valueSetter(key, value);
    }
    _streamCtrl.add(value);
  }

  @override
  T? getValue() => valueGetter(key);

  @override
  Stream<T?> changes() => _streamCtrl.stream;

  @override
  void dispose() {
    _streamCtrl.close();
  }
}
