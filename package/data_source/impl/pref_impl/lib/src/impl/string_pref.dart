import 'package:pref_impl/src/shared_pref.dart';
import 'package:shared_preferences/shared_preferences.dart';

class StringPref extends SharedPref<String> {
  StringPref(SharedPreferences sharedPreferences, String key)
      : super(
          sharedPreferences,
          key,
          sharedPreferences.setString,
          sharedPreferences.getString,
        );
}
