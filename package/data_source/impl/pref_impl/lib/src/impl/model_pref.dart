import 'package:core/export.dart';

import 'package:pref_impl/src/impl/json_pref.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../shared_pref.dart';

class ModelPref<T> extends SharedPref<T> {
  ModelPref(
    SharedPreferences sharedPreferences,
    String key,
    T Function(Map<String, dynamic>) toModel,
    Map<String, dynamic> Function(T) toJson,
  ) : super(
          sharedPreferences,
          key,
          (key, value) => sharedPreferences.setJson(key, toJson(value)),
          (key) => sharedPreferences.getJson(key)?.let(toModel),
        );
}
