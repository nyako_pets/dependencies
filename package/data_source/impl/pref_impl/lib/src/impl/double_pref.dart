import 'package:pref_impl/src/shared_pref.dart';
import 'package:shared_preferences/shared_preferences.dart';

class DoublePref extends SharedPref<double> {
  DoublePref(SharedPreferences sharedPreferences, String key)
      : super(
          sharedPreferences,
          key,
          sharedPreferences.setDouble,
          sharedPreferences.getDouble,
        );
}
