import 'package:pref_impl/src/shared_pref.dart';
import 'package:shared_preferences/shared_preferences.dart';

class IntPref extends SharedPref<int> {
  IntPref(SharedPreferences sharedPreferences, String key)
      : super(
          sharedPreferences,
          key,
          sharedPreferences.setInt,
          sharedPreferences.getInt,
        );
}
