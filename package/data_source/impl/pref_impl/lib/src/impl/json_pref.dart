import 'dart:convert';

import 'package:core/export.dart';
import 'package:pref_impl/src/shared_pref.dart';
import 'package:shared_preferences/shared_preferences.dart';

class JsonPref extends SharedPref<Map<String, dynamic>> {
  JsonPref(SharedPreferences sharedPreferences, String key)
      : super(
          sharedPreferences,
          key,
          sharedPreferences.setJson,
          sharedPreferences.getJson,
        );
}

extension SharedPreferencesJson on SharedPreferences {
  Map<String, dynamic>? getJson(String key) =>
      getString(key)?.let((value) => jsonDecode(value));

  Future<bool> setJson(String key, Map<String, dynamic> value) =>
      setString(key, jsonEncode(value));
}
