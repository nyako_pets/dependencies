import 'package:pref_impl/src/shared_pref.dart';
import 'package:shared_preferences/shared_preferences.dart';

class BoolPref extends SharedPref<bool> {
  BoolPref(SharedPreferences sharedPreferences, String key)
      : super(
          sharedPreferences,
          key,
          sharedPreferences.setBool,
          sharedPreferences.getBool,
        );
}
