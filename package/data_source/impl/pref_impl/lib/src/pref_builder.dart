import 'dart:core' as core;
import 'dart:core';

import 'package:core/export.dart';
import 'package:data_api/export.dart';
import 'package:pref_impl/src/impl/int_pref.dart';
import 'package:pref_impl/src/impl/model_pref.dart';
import 'package:pref_impl/src/shared_pref.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'impl/bool_pref.dart';
import 'impl/double_pref.dart';
import 'impl/json_pref.dart';
import 'impl/string_pref.dart';

class PrefBuilder implements Disposable {
  final SharedPreferences sharedPreferences;
  final String key;
  final _prefs = [];

  PrefBuilder(this.sharedPreferences, this.key);

  DataController<core.bool?> get bool =>
      BoolPref(sharedPreferences, key).also(_prefs.add);

  DataController<core.double?> get double =>
      DoublePref(sharedPreferences, key).also(_prefs.add);

  DataController<core.int?> get int =>
      IntPref(sharedPreferences, key).also(_prefs.add);

  DataController<Map<String, dynamic>?> get json =>
      JsonPref(sharedPreferences, key).also(_prefs.add);

  DataController<String?> get string =>
      StringPref(sharedPreferences, key).also(_prefs.add);

  DataController<T?> model<T>(
    T Function(Map<String, dynamic>) toModel,
    Map<String, dynamic> Function(T) toJson,
  ) =>
      ModelPref(sharedPreferences, key, toModel, toJson).also(_prefs.add);

  @override
  void dispose() {
    for (var pref in _prefs) {
      pref.dispose();
    }
    _prefs.clear();
  }
}
