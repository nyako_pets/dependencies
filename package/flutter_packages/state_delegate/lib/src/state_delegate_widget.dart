import 'dart:math';

import 'package:flutter/material.dart';

part 'state_delegate.dart';

class StateDelegateWidget extends StatefulWidget {
  final List<Delegate> delegates;
  final Widget Function(
    BuildContext context,
    List values,
  ) builder;

  const StateDelegateWidget({
    required this.delegates,
    required this.builder,
    super.key,
  });

  static single<T>({
    required Delegate<T> delegate,
    required Widget Function(
      BuildContext context,
      T value,
    )
        builder,
  }) {
    return StateDelegateWidget(
      delegates: [delegate],
      builder: (context, values) {
        return builder(context, values.first as T);
      },
    );
  }

  @override
  State<StateDelegateWidget> createState() => _StateDelegateWidgetState();
}

class _StateDelegateWidgetState extends State<StateDelegateWidget> {
  final List<DelegateState> _stateDelegates = [];

  _StateDelegateWidgetState();

  _updateDelegate(List<Delegate> oldDelegates) {
    final length = max(oldDelegates.length, widget.delegates.length);
    for (var i = 0; i < length; i++) {
      final oldDelegate = oldDelegates.getOrNull(i);
      final newDelegate = widget.delegates.getOrNull(i);
      if (oldDelegate != null && newDelegate != null) {
        if (Delegate.canUpdate(oldDelegate, newDelegate)) {
          final stateDelegate = _stateDelegates.getOrNull(i);
          assert(stateDelegate != null);
          _updateDelegateState(stateDelegate!, newDelegate);
        } else {
          _disposeDelegateState(_stateDelegates.removeAt(i));
          assert(_stateDelegates.getOrNull(i) == null);
          _stateDelegates.put(i, _createDelegateState(newDelegate));
        }
      } else if (oldDelegate != null && newDelegate == null) {
        _disposeDelegateState(_stateDelegates.removeAt(i));
      } else if (newDelegate != null && oldDelegate == null) {
        assert(_stateDelegates.getOrNull(i) == null);
        _stateDelegates.put(i, _createDelegateState(newDelegate));
      }
    }
  }

  DelegateState _createDelegateState(Delegate delegate) {
    final stateDelegate = delegate.createDelegate();
    stateDelegate._parentState = this;
    stateDelegate._delegate = delegate;
    stateDelegate._notifier.addListener(_onDelegateChanged);
    return stateDelegate;
  }

  void _updateDelegateState(
      DelegateState stateDelegate, Delegate newDelegate) {
    final oldDelegate = stateDelegate._delegate;
    assert(oldDelegate != null);
    stateDelegate._delegate = newDelegate;
    stateDelegate.didUpdateDelegate(oldDelegate!);
  }

  void _disposeDelegateState(DelegateState stateDelegate) {
    stateDelegate.dispose();
    stateDelegate._notifier.removeListener(_onDelegateChanged);
    stateDelegate._parentState = null;
    stateDelegate._delegate = null;
  }

  @override
  void initState() {
    _updateDelegate([]);
    super.initState();
    for (final delegate in _stateDelegates) {
      delegate.initState();
    }
  }

  @override
  void dispose() {
    super.dispose();
    for (final delegate in _stateDelegates) {
      delegate.dispose();
    }
  }

  _onDelegateChanged() {
    setState(() {});
  }

  @override
  void didUpdateWidget(StateDelegateWidget oldWidget) {
    super.didUpdateWidget(oldWidget);
    _updateDelegate(oldWidget.delegates);
  }

  @override
  Widget build(BuildContext context) {
    final values = _stateDelegates.map((e) => e._notifier.value).toList();
    return widget.builder(context, values);
  }
}

extension _ListGetter<T> on List<T> {
  T? getOrNull(int index) {
    if (index >= length || index < 0) {
      return null;
    }
    return this[index];
  }

  void put(int index, T value) {
    assert(index <= length);
    if (length == index) {
      add(value);
    } else {
      this[index] = value;
    }
  }
}
