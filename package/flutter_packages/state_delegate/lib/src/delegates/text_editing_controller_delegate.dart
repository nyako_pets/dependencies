import 'package:flutter/cupertino.dart';
import 'package:state_delegate/src/state_delegate_widget.dart';

class TextEditingControllerDelegate extends Delegate<TextEditingController> {
  final String initialText;

  TextEditingControllerDelegate(this.initialText);

  @override
  DelegateState<TextEditingController, Delegate<TextEditingController>>
      createDelegate() => _TextEditingControllerDelegatorState();
}

class _TextEditingControllerDelegatorState extends DelegateState<
    TextEditingController, TextEditingControllerDelegate> {
  @override
  TextEditingController createValue() {
    return TextEditingController(text: delegate.initialText);
  }
}
