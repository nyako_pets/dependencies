part of 'state_delegate_widget.dart';

abstract class Delegate<T> {
  final Key? key;

  const Delegate({this.key});

  @protected
  DelegateState<T, Delegate<T>> createDelegate();

  static bool canUpdate(Delegate oldDelegate, Delegate newDelegate) {
    return oldDelegate.runtimeType == newDelegate.runtimeType &&
        oldDelegate.key == newDelegate.key;
  }
}

abstract class DelegateState<T, S extends Delegate<T>> {
  late final _notifier = ValueNotifier(createValue());
  _StateDelegateWidgetState? _parentState;
  S? _delegate;

  S get delegate => _delegate!;

  BuildContext get context => _parentState!.context;

  DelegateState();

  T get value => _notifier.value;

  set value(T value) => _notifier.value = value;

  T createValue();

  void didUpdateDelegate(covariant Delegate oldDelegate) {}

  void initState() {}

  @mustCallSuper
  void dispose() {}

  void setState() {}
}
