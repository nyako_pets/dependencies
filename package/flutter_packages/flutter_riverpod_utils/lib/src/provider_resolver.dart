import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class ProviderResolver extends ConsumerWidget {
  final List<ProviderListenable<Object?>> providers;
  final ProviderResolverBuilder builder;

  const ProviderResolver._({
    required this.providers,
    required this.builder,
  });

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final values = providers.map<Object?>(ref.watch).toList();
    return builder(context, values);
  }

  static ProviderResolver single<T>({
    required ProviderListenable<T> provider,
    required ProviderResolverBuilderSingle<T> builder,
  }) =>
      ProviderResolver._(
        providers: [provider],
        builder: (context, values) => builder(context, values.first as T),
      );

  static ProviderResolver values2<V1, V2>({
    required ProviderListenable<V1> provider1,
    required ProviderListenable<V2> provider2,
    required ProviderResolverBuilderValues2<V1, V2> builder,
  }) =>
      ProviderResolver._(
        providers: [
          provider1,
          provider2,
        ],
        builder: (context, values) {
          final iterator = values.iterator;
          return builder(
            context,
            iterator.getNext() as V1,
            iterator.getNext() as V2,
          );
        },
      );

  static ProviderResolver values3<V1, V2, V3>({
    required ProviderListenable<V1> provider1,
    required ProviderListenable<V2> provider2,
    required ProviderListenable<V3> provider3,
    required ProviderResolverBuilderValues3<V1, V2, V3> builder,
  }) =>
      ProviderResolver._(
        providers: [
          provider1,
          provider2,
          provider3,
        ],
        builder: (context, values) {
          final iterator = values.iterator;
          return builder(
            context,
            iterator.getNext() as V1,
            iterator.getNext() as V2,
            iterator.getNext() as V3,
          );
        },
      );

  static ProviderResolver values4<V1, V2, V3, V4>({
    required ProviderListenable<V1> provider1,
    required ProviderListenable<V2> provider2,
    required ProviderListenable<V3> provider3,
    required ProviderListenable<V4> provider4,
    required ProviderResolverBuilderValues4<V1, V2, V3, V4> builder,
  }) =>
      ProviderResolver._(
        providers: [
          provider1,
          provider2,
          provider3,
          provider4,
        ],
        builder: (context, values) {
          final iterator = values.iterator;
          return builder(
            context,
            iterator.getNext() as V1,
            iterator.getNext() as V2,
            iterator.getNext() as V3,
            iterator.getNext() as V4,
          );
        },
      );

  static ProviderResolver values5<V1, V2, V3, V4, V5>({
    required ProviderListenable<V1> provider1,
    required ProviderListenable<V2> provider2,
    required ProviderListenable<V3> provider3,
    required ProviderListenable<V4> provider4,
    required ProviderListenable<V5> provider5,
    required ProviderResolverBuilderValues5<V1, V2, V3, V4, V5> builder,
  }) =>
      ProviderResolver._(
        providers: [
          provider1,
          provider2,
          provider3,
          provider4,
          provider5,
        ],
        builder: (context, values) {
          final iterator = values.iterator;
          return builder(
            context,
            iterator.getNext() as V1,
            iterator.getNext() as V2,
            iterator.getNext() as V3,
            iterator.getNext() as V4,
            iterator.getNext() as V5,
          );
        },
      );

  static ProviderResolver values6<V1, V2, V3, V4, V5, V6>({
    required ProviderListenable<V1> provider1,
    required ProviderListenable<V2> provider2,
    required ProviderListenable<V3> provider3,
    required ProviderListenable<V4> provider4,
    required ProviderListenable<V5> provider5,
    required ProviderListenable<V6> provider6,
    required ProviderResolverBuilderValues6<V1, V2, V3, V4, V5, V6> builder,
  }) =>
      ProviderResolver._(
        providers: [
          provider1,
          provider2,
          provider3,
          provider4,
          provider5,
          provider6,
        ],
        builder: (context, values) {
          final iterator = values.iterator;
          return builder(
            context,
            iterator.getNext() as V1,
            iterator.getNext() as V2,
            iterator.getNext() as V3,
            iterator.getNext() as V4,
            iterator.getNext() as V5,
            iterator.getNext() as V6,
          );
        },
      );

  static ProviderResolver values7<V1, V2, V3, V4, V5, V6, V7>({
    required ProviderListenable<V1> provider1,
    required ProviderListenable<V2> provider2,
    required ProviderListenable<V3> provider3,
    required ProviderListenable<V4> provider4,
    required ProviderListenable<V5> provider5,
    required ProviderListenable<V6> provider6,
    required ProviderListenable<V7> provider7,
    required ProviderResolverBuilderValues7<V1, V2, V3, V4, V5, V6, V7> builder,
  }) =>
      ProviderResolver._(
        providers: [
          provider1,
          provider2,
          provider3,
          provider4,
          provider5,
          provider6,
          provider7,
        ],
        builder: (context, values) {
          final iterator = values.iterator;
          return builder(
            context,
            iterator.getNext() as V1,
            iterator.getNext() as V2,
            iterator.getNext() as V3,
            iterator.getNext() as V4,
            iterator.getNext() as V5,
            iterator.getNext() as V6,
            iterator.getNext() as V7,
          );
        },
      );

  static ProviderResolver values8<V1, V2, V3, V4, V5, V6, V7, V8>({
    required ProviderListenable<V1> provider1,
    required ProviderListenable<V2> provider2,
    required ProviderListenable<V3> provider3,
    required ProviderListenable<V4> provider4,
    required ProviderListenable<V5> provider5,
    required ProviderListenable<V6> provider6,
    required ProviderListenable<V7> provider7,
    required ProviderListenable<V8> provider8,
    required ProviderResolverBuilderValues8<V1, V2, V3, V4, V5, V6, V7, V8>
    builder,
  }) =>
      ProviderResolver._(
        providers: [
          provider1,
          provider2,
          provider3,
          provider4,
          provider5,
          provider6,
          provider7,
          provider8,
        ],
        builder: (context, values) {
          final iterator = values.iterator;
          return builder(
            context,
            iterator.getNext() as V1,
            iterator.getNext() as V2,
            iterator.getNext() as V3,
            iterator.getNext() as V4,
            iterator.getNext() as V5,
            iterator.getNext() as V6,
            iterator.getNext() as V7,
            iterator.getNext() as V8,
          );
        },
      );

  static ProviderResolver values9<V1, V2, V3, V4, V5, V6, V7, V8, V9>({
    required ProviderListenable<V1> provider1,
    required ProviderListenable<V2> provider2,
    required ProviderListenable<V3> provider3,
    required ProviderListenable<V4> provider4,
    required ProviderListenable<V5> provider5,
    required ProviderListenable<V6> provider6,
    required ProviderListenable<V7> provider7,
    required ProviderListenable<V8> provider8,
    required ProviderListenable<V9> provider9,
    required ProviderResolverBuilderValues9<V1, V2, V3, V4, V5, V6, V7, V8, V9>
    builder,
  }) =>
      ProviderResolver._(
        providers: [
          provider1,
          provider2,
          provider3,
          provider4,
          provider5,
          provider6,
          provider7,
          provider8,
          provider9,
        ],
        builder: (context, values) {
          final iterator = values.iterator;
          return builder(
            context,
            iterator.getNext() as V1,
            iterator.getNext() as V2,
            iterator.getNext() as V3,
            iterator.getNext() as V4,
            iterator.getNext() as V5,
            iterator.getNext() as V6,
            iterator.getNext() as V7,
            iterator.getNext() as V8,
            iterator.getNext() as V9,
          );
        },
      );

  static ProviderResolver values10<V1, V2, V3, V4, V5, V6, V7, V8, V9, V10>({
    required ProviderListenable<V1> provider1,
    required ProviderListenable<V2> provider2,
    required ProviderListenable<V3> provider3,
    required ProviderListenable<V4> provider4,
    required ProviderListenable<V5> provider5,
    required ProviderListenable<V6> provider6,
    required ProviderListenable<V7> provider7,
    required ProviderListenable<V8> provider8,
    required ProviderListenable<V9> provider9,
    required ProviderListenable<V10> provider10,
    required ProviderResolverBuilderValues10<V1, V2, V3, V4, V5, V6, V7, V8, V9,
        V10>
    builder,
  }) =>
      ProviderResolver._(
        providers: [
          provider1,
          provider2,
          provider3,
          provider4,
          provider5,
          provider6,
          provider7,
          provider8,
          provider9,
          provider10,
        ],
        builder: (context, values) {
          final iterator = values.iterator;
          return builder(
            context,
            iterator.getNext() as V1,
            iterator.getNext() as V2,
            iterator.getNext() as V3,
            iterator.getNext() as V4,
            iterator.getNext() as V5,
            iterator.getNext() as V6,
            iterator.getNext() as V7,
            iterator.getNext() as V8,
            iterator.getNext() as V9,
            iterator.getNext() as V10,
          );
        },
      );
}

typedef ProviderResolverBuilder = Widget Function(BuildContext, List<Object?>);
typedef ProviderResolverBuilderSingle<T> = Widget Function(BuildContext, T);
typedef ProviderResolverBuilderValues2<V1, V2> = Widget Function(
    BuildContext,
    V1,
    V2,
    );
typedef ProviderResolverBuilderValues3<V1, V2, V3> = Widget Function(
    BuildContext,
    V1,
    V2,
    V3,
    );
typedef ProviderResolverBuilderValues4<V1, V2, V3, V4> = Widget Function(
    BuildContext,
    V1,
    V2,
    V3,
    V4,
    );
typedef ProviderResolverBuilderValues5<V1, V2, V3, V4, V5> = Widget Function(
    BuildContext,
    V1,
    V2,
    V3,
    V4,
    V5,
    );
typedef ProviderResolverBuilderValues6<V1, V2, V3, V4, V5, V6> = Widget
Function(
    BuildContext,
    V1,
    V2,
    V3,
    V4,
    V5,
    V6,
    );
typedef ProviderResolverBuilderValues7<V1, V2, V3, V4, V5, V6, V7> = Widget
Function(BuildContext, V1, V2, V3, V4, V5, V6, V7);
typedef ProviderResolverBuilderValues8<V1, V2, V3, V4, V5, V6, V7, V8> = Widget
Function(BuildContext, V1, V2, V3, V4, V5, V6, V7, V8);
typedef ProviderResolverBuilderValues9<V1, V2, V3, V4, V5, V6, V7, V8, V9>
= Widget Function(BuildContext, V1, V2, V3, V4, V5, V6, V7, V8, V9);
typedef ProviderResolverBuilderValues10<V1, V2, V3, V4, V5, V6, V7, V8, V9, V10>
= Widget Function(BuildContext, V1, V2, V3, V4, V5, V6, V7, V8, V9, V10);

extension _Iterator<T> on Iterator<T> {
  T getNext() {
    moveNext();
    return current;
  }
}
