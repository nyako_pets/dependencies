import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_core/flutter_core.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(body: Page()),
    );
  }
}

class Page extends StatefulWidget {
  @override
  State<Page> createState() => _PageState();
}

class _PageState extends State<Page> {
  void _onError(Object error, StackTrace stack) {
    print(error);
  }

  void _onError1(Object error, StackTrace stack) {
    print(error);
  }

  @override
  Widget build(BuildContext context) {
    Future.error('uncatched');
    return Center(
      child: ZonedGuardedWidget(
        onError: _onError,
        builder: (context) {
          return GestureDetector(
            onTap: () {
              Future.error('red');
            },
            child: Container(
              width: 300,
              height: 300,
              color: Colors.red,
              child: Center(
                child: ZonedGuardedWidget(
                  onError: _onError1,
                  builder: (context) {
                    return GestureDetector(
                      onTap: () {
                        Future.error('green');
                      },
                      child: Container(
                        width: 100,
                        height: 100,
                        color: Colors.green,
                      ),
                    );
                  },
                ),
              ),
            ),
          );
        },
      ),
    );
  }
}
