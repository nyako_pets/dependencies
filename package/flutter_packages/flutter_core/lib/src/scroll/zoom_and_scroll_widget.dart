import 'package:flutter/material.dart';

class ZoomAndScrollWidget extends StatefulWidget {
  final int itemCount;
  final Widget Function(BuildContext, int) itemBuilder;

  const ZoomAndScrollWidget({
    Key? key,
    required this.itemBuilder,
    required this.itemCount,
  }) : super(key: key);

  @override
  State<ZoomAndScrollWidget> createState() => _ZoomAndScrollWidgetState();
}

class _ZoomAndScrollWidgetState extends State<ZoomAndScrollWidget> {
  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (BuildContext context, BoxConstraints constraints) {
        final size = constraints.biggest;
        return InteractiveViewer.builder(
          builder: (context, viewport) {
            return SizedBox(
              width: size.width,
              child: Column(
                children: [
                  for (var i = 0; i < widget.itemCount; i++)
                    widget.itemBuilder(context, i),
                ],
              ),
            );
          },
        );
      },
    );
  }
}
