import 'dart:async';

import 'package:flutter/cupertino.dart';

class ZonedGuardedWidget extends StatefulWidget {
  final void Function(Object error, StackTrace stack) onError;
  final Map<Object?, Object?>? zoneValues;
  final ZoneSpecification? zoneSpecification;
  final WidgetBuilder builder;

  const ZonedGuardedWidget({
    Key? key,
    required this.onError,
    required this.builder,
    this.zoneValues,
    this.zoneSpecification,
  }) : super(key: key);

  @override
  State<ZonedGuardedWidget> createState() => _ZonedGuardedWidgetState();
}

class _ZonedGuardedWidgetState extends State<ZonedGuardedWidget> {
  late Zone? zone = _initZone();

  @override
  void didUpdateWidget(covariant ZonedGuardedWidget oldWidget) {
    super.didUpdateWidget(oldWidget);
    if (widget.onError != oldWidget.onError ||
        widget.zoneValues != oldWidget.zoneValues ||
        widget.zoneSpecification != oldWidget.zoneSpecification) {
      zone = _initZone();
    }
  }

  @override
  Widget build(BuildContext context) {
    return zone?.runUnary(widget.builder, context) ?? widget.builder(context);
  }

  Zone? _initZone() {
    final parentZone = Zone.current;
    HandleUncaughtErrorHandler errorHandler = (Zone self, ZoneDelegate parent,
        Zone zone, Object error, StackTrace stackTrace) {
      try {
        parentZone.runBinary(widget.onError, error, stackTrace);
      } catch (e, s) {
        if (identical(e, error)) {
          parent.handleUncaughtError(zone, error, stackTrace);
        } else {
          parent.handleUncaughtError(zone, e, s);
        }
      }
    };
    ZoneSpecification? zoneSpecification = widget.zoneSpecification;
    if (zoneSpecification == null) {
      zoneSpecification =
          new ZoneSpecification(handleUncaughtError: errorHandler);
    } else {
      zoneSpecification = ZoneSpecification.from(zoneSpecification,
          handleUncaughtError: errorHandler);
    }
    try {
      return Zone.current.fork(
          specification: zoneSpecification, zoneValues: widget.zoneValues);
    } catch (error, stackTrace) {
      widget.onError(error, stackTrace);
    }
    return null;
  }
}
