import '../../export.dart';

mixin ById<T> {}

mixin GetOptionalById<ID, T> on DataApi<T>, ById<ID> {
  Future<T?> getOptionalById(ID id);
}
mixin GetById<ID, T> on DataApi<T>, ById<ID> implements GetOptionalById<ID, T>{
  Future<T> getById(ID id);

  Future<T?> getOptionalById(ID id) => getById(id);
}

mixin ListenOptionalById<ID, T> on DataApi<T>, ById<ID> {
  Stream<T?> listenOptionalById(ID id);
}

mixin ListenById<ID, T> on DataApi<T>, ById<ID> implements
    ListenOptionalById<ID, T>{
  Stream<T> listenById(ID id);

  Stream<T?> listenOptionalById(ID id) => listenById(id);
}
mixin DeleteById<ID, T> on DataApi<T>, ById<ID> {
  Future<void> deleteById(ID id);
}
mixin DeleteByIds<ID, T> on DataApi<T>, ById<ID> {
  Future<void> deleteByIds(Iterable<ID> id);
}
mixin ExistById<ID, T> on DataApi<T>, ById<ID> {
  Future<bool> existById(ID id);
}
