import 'package:data_api/export.dart';

abstract class ByArgResult<T> {
  Iterable<T> get items;
}

mixin ByArg<A, R extends ByArgResult<T>, T> on DataApi<T> {}

mixin GetByArg<A, R extends ByArgResult<T>, T> on ByArg<A, R, T>, DataApi<T> {
  Future<R> getByArg(A argument);
}

mixin ListenByArg<A, R extends ByArgResult<T>, T> on ByArg<A, R, T>, DataApi<T> {
  Stream<R> listenByArg(A argument);
}
