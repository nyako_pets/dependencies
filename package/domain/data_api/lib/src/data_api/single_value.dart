import 'package:data_api/export.dart';

import 'data_api.dart';

mixin DataSetter<T> on DataApi<T> {
  void setValue(T value);
}

mixin DataGetter<T> on DataApi<T> {
  T getValue();
}

mixin DataStream<T> on DataApi<T> {
  Stream<T> changes();
}

abstract class DataHolder<T> implements DataSetter<T>, DataGetter<T> {}

abstract class DataSubject<T> implements DataStream<T>, DataGetter<T> {}

abstract class DataController<T> implements DataSubject<T>, DataHolder<T> {}
