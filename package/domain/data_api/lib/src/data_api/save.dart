import 'package:data_api/export.dart';

mixin Save<T> on DataApi<T> {
  Future<void> save(T model);
}

mixin SaveAll<T> on DataApi<T> {
  Future<void> saveAll(Iterable<T> models);
}
