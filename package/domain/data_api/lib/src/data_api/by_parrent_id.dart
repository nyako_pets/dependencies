import 'package:data_api/export.dart';

mixin ByParentId<T> {}

mixin GetByParentId<ID, T> on DataApi<T>, ByParentId<ID> {
  Future<List<T>> getByParentId(ID id);
}
mixin DeleteByParentId<ID, T> on DataApi<T>, ByParentId<ID> {
  Future<void> deleteByParentId(ID id);
}
mixin ListenByParentId<ID, T> on DataApi<T>, ByParentId<ID> {
  Stream<List<T>> listenByParentId(ID id);
}