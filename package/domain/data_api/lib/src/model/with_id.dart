abstract class WithId<T> {
  T get id;
}
