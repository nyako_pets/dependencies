abstract class WithParentId<T> {
  T get parentId;
}
