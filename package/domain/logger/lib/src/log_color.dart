class LogColor {
  final String value;

  const LogColor(this.value);

  static const black = LogColor('\x1B[30m');
  static const red = LogColor('\x1B[31m');
  static const green = LogColor('\x1B[32m');
  static const yellow = LogColor('\x1B[33m');
  static const blue = LogColor('\x1B[34m');
  static const magenta = LogColor('\x1B[35m');
  static const cyan = LogColor('\x1B[36m');
  static const white = LogColor('\x1B[37m');
  static const reset = LogColor('\x1B[0m');

  String wrap(String text) => '${value}$text${reset.value}';
}
