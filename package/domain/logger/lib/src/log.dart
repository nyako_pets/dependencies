import 'package:logger/export.dart';

class Log {
  final LogType logType;
  final String message;
  final StackTrace? stackTrace;
  final Object? error;

  Log(this.logType, this.message, this.stackTrace, this.error);
}
