import 'package:logger/src/log.dart';
import 'package:logger/src/log_color.dart';

final logger = Logger._();

class Logger {
  void Function(Log)? overridePrint;

  Logger._();

  void add(Log log) {
    (overridePrint ?? _print)(log);
  }

  void _print(Log log) {
    if (log.stackTrace != null) {
      print(LogColor.blue.wrap(log.stackTrace.toString()));
    }
    if (log.error != null) {
      print(LogColor.red.wrap(log.error.toString()));
    }
    print('[${log.logType}] ${log.message}');
  }
}
