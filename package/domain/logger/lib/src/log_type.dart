import '../export.dart';
import 'log.dart';
import 'log_color.dart';

class LogType {
  final String text;
  final LogColor color;

  const LogType(this.text, this.color);

  void add(String message, {StackTrace? stackTrace, Object? error}) =>
      logger.add(Log(this, message, stackTrace, error));

  @override
  String toString() => color.wrap(text);
}
