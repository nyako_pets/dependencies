class LockInitiator {
  final Object _tag;

  const LockInitiator(this._tag);

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
          other is LockInitiator &&
              runtimeType == other.runtimeType &&
              _tag == other._tag;

  @override
  int get hashCode => _tag.hashCode;
}
