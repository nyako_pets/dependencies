import 'package:rxdart/rxdart.dart';

import 'locker.dart';
import 'locker_tag.dart';

class LockerByAny extends Locker {
  final _subject = BehaviorSubject.seeded(<LockInitiator>{});

  @override
  bool get isLocked => _subject.value.isNotEmpty;

  @override
  Stream<bool> get isLockedStream =>
      _subject.map((event) => event.isNotEmpty).distinct();

  @override
  void lockBy(LockInitiator tag) {
    _update((tags) => tags.add(tag));
  }

  @override
  void unlockBy(LockInitiator tag) {
    _update((tags) => tags.remove(tag));
  }

  @override
  void clear() {
    _update((tags) => tags.clear());
  }

  void _update(void Function(Set<LockInitiator>) onUpdate) {
    final value = _subject.value;
    onUpdate(value);
    _subject.add(value);
  }
}
