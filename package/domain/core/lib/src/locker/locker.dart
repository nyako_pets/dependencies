import 'package:rxdart/rxdart.dart';

import 'locker_tag.dart';

abstract class Locker {
  bool get isLocked;

  Stream<bool> get isLockedStream;

  void lockBy(LockInitiator tag);

  void unlockBy(LockInitiator tag);

  void clear();
}
