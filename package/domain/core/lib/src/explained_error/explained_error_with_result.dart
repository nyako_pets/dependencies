import 'explained_error.dart';

abstract class ExplainedErrorWithResult<T> extends ExplainedError {
  final T result;

  ExplainedErrorWithResult({
    required this.result,
    required super.error,
    required super.stackTrace,
  });

  T toReturn() {
    throw this;
  }
}
