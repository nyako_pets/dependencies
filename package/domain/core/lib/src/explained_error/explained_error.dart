import 'package:core/src/extension/iterable_extension.dart';

import 'explained_error_with_result.dart';

abstract class ExplainedError extends Error {
  final Object error;
  final StackTrace stackTrace;

  ExplainedError({
    required this.error,
    required this.stackTrace,
  });

  Iterable<Object> get errorStack sync* {
    ExplainedError error = this;
    while (true) {
      yield error;
      final childError = error.error;
      if (childError is ExplainedError) {
        error = childError;
        continue;
      } else {
        yield childError;
        return;
      }
    }
  }

  Object get reason {
    final error = this.error;
    if (error is ExplainedError) {
      return error.reason;
    } else {
      return error;
    }
  }

  StackTrace get reasonStackTrace {
    final error = this.error;
    if (error is ExplainedError) {
      return error.reasonStackTrace;
    } else {
      return stackTrace;
    }
  }

  T getResult<T>() => getAllResults<T>().first;

  Iterable<T> getAllResults<T>() => errorStack.map<T?>((e) {
        if (e is ExplainedErrorWithResult<T>) {
          return e.result;
        } else {
          return null;
        }
      }).notNull();
}
