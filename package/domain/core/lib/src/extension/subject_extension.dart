import 'package:core/src/extension/stream_extension.dart';
import 'package:data_api/export.dart';
import 'package:rxdart/rxdart.dart';

mixin SubjectExtension<T> on Subject<T> {
  DataStream<T> toDataStream() {
    return stream.toDataStream();
  }
}

mixin BehaviorSubjectExtension<T> on BehaviorSubject<T> {
  DataController<T> toDataController() {
    return _DataSubject(this);
  }
}

class _DataSubject<T> extends DataController<T> {
  final BehaviorSubject<T> _stream;

  _DataSubject(this._stream);

  @override
  Stream<T> changes() {
    return _stream;
  }

  @override
  T getValue() {
    return _stream.value;
  }

  @override
  void setValue(T value) {
    _stream.add(value);
  }
}
