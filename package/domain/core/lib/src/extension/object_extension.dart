extension ObjectExtension<T> on T {
  O let<O>(O Function(T) bloc) => bloc(this);

  T also(void Function(T) bloc) {
    bloc(this);
    return this;
  }
}
