import 'package:logger/export.dart';

const _tryCallLogger = LogType("TryCall", LogColor.yellow);
const _message = "";

extension TryCall0<T> on T Function() {
  T? tryCall() {
    try {
      return call();
    } catch (e, s) {
      _tryCallLogger.add(_message, error: e, stackTrace: s);
      return null;
    }
  }
}

extension TryCall1<T, A> on T Function(A) {
  T? tryCall(A a) {
    try {
      return call(a);
    } catch (e, s) {
      _tryCallLogger.add(_message, error: e, stackTrace: s);
      return null;
    }
  }
}

extension TryCall2<T, A1, A2> on T Function(A1, A2) {
  T? tryCall(A1 a1, A2, a2) {
    try {
      return call(a1, a2);
    } catch (e, s) {
      _tryCallLogger.add(_message, error: e, stackTrace: s);
      return null;
    }
  }
}

extension TryCall3<T, A1, A2, A3> on T Function(A1, A2, A3) {
  T? tryCall(A1 a1, A2 a2, A3 a3) {
    try {
      return call(a1, a2, a3);
    } catch (e, s) {
      _tryCallLogger.add(_message, error: e, stackTrace: s);
      return null;
    }
  }
}

extension TryCall$<T, A1, A2, A3, A4> on T Function(A1, A2, A3, A4) {
  T? tryCall(A1 a1, A2 a2, A3 a3, A4 a4) {
    try {
      return call(a1, a2, a3, a4);
    } catch (e, s) {
      _tryCallLogger.add(_message, error: e, stackTrace: s);
      return null;
    }
  }
}
