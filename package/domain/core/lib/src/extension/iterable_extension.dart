extension NullableIterableExtension<T> on Iterable<T?> {
  Iterable<T> notNull() => where((item) => item != null).cast();
}
