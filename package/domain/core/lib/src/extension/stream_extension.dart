import 'package:data_api/export.dart';

extension NullableStreamExtension<T> on Stream<T?> {
  Stream<T> notNull() => where((item) => item != null).cast();
}

extension StreamExtension<T> on Stream<T> {
  DataStream<T> toDataStream() {
    return _DataStream(this);
  }
}

class _DataStream<T> extends DataApi<T> with DataStream {
  final Stream<T> _stream;

  _DataStream(this._stream);

  @override
  Stream<T> changes() {
    return _stream;
  }
}
